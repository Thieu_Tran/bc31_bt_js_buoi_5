// Bài 1: Quản lý tuyển sinh

document.getElementById("btn-bai1").addEventListener("click",function(){

    var diemMon1 = document.getElementById("num1").value*1;
    var diemMon2 = document.getElementById("num2").value*1;
    var diemMon3 = document.getElementById("num3").value*1;
    var diemChuanValue = document.getElementById("diemChuan").value*1;

    var getIdKhuVuc = document.getElementById("khu-vuc");
    var textKhuVuc = getIdKhuVuc.options[getIdKhuVuc.selectedIndex].text;
    var diemKhuVucValue = null;

    var getIdDoiTuong = document.getElementById("doi-tuong");
    var textDoiTuong = getIdDoiTuong.options[getIdDoiTuong.selectedIndex].text;
    var diemDoiTuongValue = null;

    var tongDiem = null;

    // Lấy điểm khu vực
    switch(textKhuVuc){
        case "A":
            diemKhuVucValue = 2;
            break;
        case "B":
            diemKhuVucValue = 1;
            break;
        case "C":
            diemKhuVucValue = 0.5;
            break;
        default:
            diemKhuVucValue = 0;
    }
    // Lấy điểm đối tượng
    switch(textDoiTuong){
        case "1":
            diemDoiTuongValue = 2.5;
            break;
        case "2":
            diemDoiTuongValue = 1.5;
            break;
        case "3":
            diemDoiTuongValue = 1;
            break;
        default:
            diemDoiTuongValue = 0;
    }
    
    tongDiem = diemMon1 + diemMon2 + diemMon3 + diemKhuVucValue + diemDoiTuongValue;
    if(diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0){
        document.getElementById("kq1").innerHTML = "Bạn đã rớt do có điểm môn bất kỳ bằng 0";
    }
    else if(tongDiem >= diemChuanValue){
        document.getElementById("kq1").innerHTML = `Bạn đã đậu. Tổng điểm: ${tongDiem}`;
    }
    else{
        document.getElementById("kq1").innerHTML = `Bạn đã rớt. Tổng điểm: ${tongDiem}`;
    }
})

// Bài 2: Tính tiền điện
document.getElementById("btn-bai2").addEventListener("click",function(){
    var nameText = document.getElementById("txt-name").value;
    var kwValue = document.getElementById("txt-kw").value*1;

    var tongTien = null;
    const donGia1 = 500;
    const donGia2 = 650;
    const donGia3 = 850;
    const donGia4 = 1100;
    const donGia5 = 1300;

    if(kwValue <= 50){
        tongTien = kwValue*donGia1;
    }
    else if(kwValue <= 100){
        tongTien = 50*donGia1 + (kwValue-50)*donGia2;
    }
    else if(kwValue <= 200){
        tongTien = 50*donGia1 + 50*donGia2 + (kwValue-100)*donGia3;
    }
    else if(kwValue <= 350){
        tongTien = 50*donGia1 + 50*donGia2 + 100*donGia3 + (kwValue-200)*donGia4;
    }
    else if(kwValue > 350){
        tongTien = 50*donGia1 + 50*donGia2 + 100*donGia3 + 150*donGia4 + (kwValue-350)*donGia5;
    }

    document.getElementById("kq2").innerHTML = `Họ tên: ${nameText} ; Tiền điện: ` + tongTien.toLocaleString();
})
